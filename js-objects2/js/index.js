function Player(user, job, level) {
    this.user = user;
    this.job = job;
    this.level = level;
    this.health = 5 * level;
    this.atttack = 1.5 * level;

    if (job == "Swordsman") {
        this.abilities = ["Sword Thrust", "Slice"];
    } else if (job == "Archer") {
        this.abilities = ["Dance of Death", "Raining Arrows"]
    } else if (job == "Mage") {
        this.abilities = ["Freezing field", "Fireball"]
    }
}

let char1 = new Player("DriveThru", "Archer", 105);
let char2 = new Player("stickFiggas", "Mage", 87);
let char3 = new Player("rimuru144", "Archer", 95);

console.log(char1);
console.log(char2);
console.log(char3);