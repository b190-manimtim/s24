let getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const myAddress = ['258 Washington Ave NW', "California 90011"];
const [address, state] = myAddress;

// using template literals
console.log(`I live at ${address}, ${state}`);

// ANIMAL - OBJECT DESTRUCTURING
const animal = {
    name: "Lolong",
    typeOfAnimal: "saltwater crocodile",
    weight: 1075,
    measurement: "20 ft 3 in"
}

const { name, typeOfAnimal, weight, measurement } = animal;
console.log(`${name} was a ${typeOfAnimal}. He weighed ${weight} kgs with a measurement of ${measurement}.`);

// ARROW FUNCTION
const numbers = [1, 2, 3, 4, 5];
// pre-arrow function
numbers.forEach(function(number) {
    console.log(`${number}`);
});

// REDUCE
const initialValue = 0;
const reduceNumber = numbers.reduce(
    (previousValue, currentValue) => previousValue + currentValue,
    initialValue
);

console.log(reduceNumber);

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog("Cloud", 2, "Pomeranian")
console.log(myDog);